import AppForm from '../app-components/Form/AppForm';

Vue.component('task-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                assignee:  '' ,
                task:  '' ,
                status:  '' ,
                
            }
        }
    }

});