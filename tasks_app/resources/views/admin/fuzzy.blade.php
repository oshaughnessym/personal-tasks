@extends('brackets/admin-ui::admin.layout.default')
@section('title', "Fuzzy Duck")

@section('body')
    <h1>Fuzzy Duck</h1>

    <div class="card text-center">
        <div class="card-body">
          <div class="container">
            <div class="row">
              <div class="col-sm-9">
                  <header>Code</header>
                <p style="text-align:left;"><code>
                    <p style="text-align:left;">$numbers = array();</p>

                    <p style="text-align:left;">for ($i=1; $i < 101 ; $i++) { <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;$fuzzyduck = strval($i);<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;if (($i % 3 ==0 || strpos($fuzzyduck, "3") !== false ) && ($i % 7 ==0 || strpos($fuzzyduck, "7") !== false)) { <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;array_push($numbers, "FuzzyDuck");
                        &nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;} elseif ($i % 3 ==0 || strpos($fuzzyduck, "3") !== false) {<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;array_push($numbers, "Fuzzy");<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;} elseif ($i % 7 ==0 || strpos($fuzzyduck, "7") !== false) { <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;array_push($numbers, "Duck");<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;} else { <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;array_push($numbers, $i);<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;}<br>
                    } </p></p>
                </code>
              </div>
              <div class="col-sm-3">
                <header>Result</header>
                <div style="max-height: 300px; width:100%; overflow: auto; display: inline-block;">
                    <table class="table table-striped table-hover">
                        <tbody>
                            <tr>
                                <td><b>Output:</b></td>
                            </tr>
                            
                            @foreach($numbers as $number)
                                <tr>    
                                    <td>{{$number}}</td>           
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
    </div>

    



@endsection