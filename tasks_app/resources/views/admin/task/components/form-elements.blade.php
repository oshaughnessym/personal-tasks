<div class="form-group row align-items-center" :class="{'has-danger': errors.has('assignee'), 'has-success': fields.assignee && fields.assignee.valid }">
    <label for="assignee" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.task.columns.assignee') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.assignee" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('assignee'), 'form-control-success': fields.assignee && fields.assignee.valid}" id="assignee" name="assignee" placeholder="{{ trans('admin.task.columns.assignee') }}">
            <option  value="Max" selected="selected">Max</option>
        </select>
        <input hidden type="text" v-model="form.assignee" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('assignee'), 'form-control-success': fields.assignee && fields.assignee.valid}" id="assignee" name="assignee" placeholder="{{ trans('admin.task.columns.assignee') }}">
        <div v-if="errors.has('assignee')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('assignee') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('task'), 'has-success': fields.task && fields.task.valid }">
    <label for="task" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.task.columns.task') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.task" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('task'), 'form-control-success': fields.task && fields.task.valid}" id="task" name="task" placeholder="{{ trans('admin.task.columns.task') }}">
        <div v-if="errors.has('task')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('task') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('status'), 'has-success': fields.status && fields.status.valid }">
    <label for="status" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.task.columns.status') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.status" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('status'), 'form-control-success': fields.status && fields.status.valid}" id="status" name="status" placeholder="{{ trans('admin.task.columns.status') }}">
            <option value="Not Started">Not Started</option>
            <option value="In Progress">In Progress</option>
            <option value="Parked">Parked</option>
            <option value="Complete">Complete</option>
        </select>
        <input hidden type="text" v-model="form.status" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('status'), 'form-control-success': fields.status && fields.status.valid}" id="status" name="status" placeholder="{{ trans('admin.task.columns.status') }}">
        <div v-if="errors.has('status')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('status') }}</div>
    </div>
</div>


