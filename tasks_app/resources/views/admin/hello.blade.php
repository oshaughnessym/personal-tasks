@extends('brackets/admin-ui::admin.layout.default')
@section('title', "Home")

@section('body')
    <h1>Dashboard Home</h1>

    <div class="row">
        <div class="col-lg-4">	
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">Pending Tasks</h5>
                  <p class="card-text">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><b>Assignee</b></td>
                                <td><b>Task</b></td>
                            </tr>
                            
                            @foreach($incomplete['notStarted'] as $data)
                                <tr>    
                                    <td>{{$data->assignee}}</td>      
                                    <td>{{$data->task}}</td>              
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </p>
                  <a href="#" class="card-link">Refresh</a>
                </div>
            </div>															
        </div>
        <div class="col-lg-4">	
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">In Progress Tasks</h5>
                  <p class="card-text">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><b>Assignee</b></td>
                                <td><b>Task</b></td>
                            </tr>
                            @foreach($inprogress['inProgress'] as $data)
                                <tr>    
                                    <td>{{$data->assignee}}</td>      
                                    <td>{{$data->task}}</td>              
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </p>                  
                  <a href="#" class="card-link">Refresh</a>
                </div>
            </div>														
        </div>
        <div class="col-lg-4">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                  <h5 class="card-title">Complete Tasks</h5>
                  <p class="card-text">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><b>Assignee</b></td>
                                <td><b>Task</b></td>
                            </tr>
                            @foreach($done['complete'] as $data)
                                <tr>    
                                    <td>{{$data->assignee}}</td>      
                                    <td>{{$data->task}}</td>              
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </p>
                  <a href="#" class="card-link">Refresh</a>
                </div>
            </div>						
        </div>
    </div>

    



@endsection