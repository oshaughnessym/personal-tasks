<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class HelloController extends Controller
{
    public function index() {

        $data = array();

        $notStarted = DB::select('select * from tasks where status ="Not Started" ');
        $progress = DB::select('select * from tasks where status ="In Progress" ');
        $complete = DB::select('select * from tasks where status ="Complete" ');

        $incomplete['notStarted'] = $notStarted;
        $inprogress['inProgress'] = $progress;
        $done['complete'] = $complete;
        
        return view('admin.hello', compact("incomplete","inprogress", "done"));
    }

    public function fizz()
    {
        $numbers = array();
        for ($i=1; $i < 101 ; $i++) { 
            if ($i % 15 ==0) {
                array_push($numbers, "FizzBuzz");
            } elseif ($i % 3 ==0) {
                array_push($numbers, "Fizz");
            } elseif ($i % 5 ==0) {
                array_push($numbers, "Buzz");
            } else {
                array_push($numbers, $i);
            }
            
        } 
        //dd($numbers);
        return view('admin.fizzbuzz', compact("numbers"));
    }

    public function fuzzy()
    {
        $numbers = array();
        for ($i=1; $i < 101 ; $i++) { 
            $fuzzyduck = strval($i);
            
            if (($i % 3 ==0 || strpos($fuzzyduck, "3") !== false ) && ($i % 7 ==0 || strpos($fuzzyduck, "7") !== false)) {
                array_push($numbers, "FuzzyDuck");
            } elseif ($i % 3 ==0 || strpos($fuzzyduck, "3") !== false) {
                array_push($numbers, "Fuzzy");
            } elseif ($i % 7 ==0 || strpos($fuzzyduck, "7") !== false) {
                array_push($numbers, "Duck");
            } else {
                array_push($numbers, $i);
            }
            
        } 
        //dd($numbers);
        return view('admin.fuzzy', compact("numbers"));
    }
}
