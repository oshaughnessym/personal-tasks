## Requirements: 
Composer - https://getcomposer.org/

Local MySQL Database (I am using XAMPP)


## Setup
1. Install composer globally using link above

2. Run a local MySQL instance and create a new database named "laravel"
Leave this empty

### Command Line Steps
<pre>$ composer global require laravel/installer

$ composer global require "brackets/craftable-installer" 

$ git clone git@gitlab.com:oshaughnessym/dev-challenge.git

$ cd dev-challenge/tasks_app

*The following commands setup project dependencies*

$ composer update
    - creates vendor package

$ php artisan migrate
    - creates database tables into already created "laravel" db

$ npm install && npm run dev

$ php artisan serv
    - run local server. localhost:8000/</pre>

## Login info:
max@test.com

test123

